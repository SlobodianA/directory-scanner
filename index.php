<?php require_once 'scanner.php'?>

<!DOCTYPE html>
<html>
<head>
    <title>File scanner</title>
</head>
<body>
<table>
    <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Filesize</th>
        <th>Modified at</th>
    </tr>
    <?php foreach ($iterator as $info): ?>
        <?php if (is_dir($info->getFilename())):?>
            <tr>
                <td><img src="icons/directory.svg" width="20"> <?=basename((str_replace('.', '', $info->getPathname())));?></td>
                <td>Directory</td>
                <td></td>
                <td></td>
            </tr>
        <?php elseif(mime_content_type($info->getPathname()) === 'text/plain'): ?>
            <tr>
                <td><img src="icons/textfile.svg" width="20"><?=basename($info->getPathname());?></td>
                <td><?='TXT'?></td>
                <td><?=number_format((float)filesize($info->getPathname())/1024, 2, '.', ''). ' Kb';?></td>
                <td><?=date("F d Y H:i:s.", fileatime($info->getPathname()))?></td>
            </tr>
        <?php elseif(mime_content_type($info->getPathname()) === 'image/png'): ?>
            <tr>
                <td><img src="icons/image_png.svg" width="20"><?=basename($info->getPathname());?></td>
                <td><?='PNG'?></td>
                <td><?=number_format((float)filesize($info->getPathname())/1024, 2, '.', ''). ' Kb';?></td>
                <td><?=date("F d Y H:i:s.", fileatime($info->getPathname()))?></td>
            </tr>
        <?php elseif(mime_content_type($info->getPathname()) === 'image/jpeg'): ?>
            <tr>
                <td><img src="icons/image_jpg.svg" width="20"><?=basename($info->getPathname());?></td>
                <td><?='JPG'?></td>
                <td><?=number_format((float)filesize($info->getPathname())/1024, 2, '.', ''). ' Kb';?></td>
                <td><?=date("F d Y H:i:s.", fileatime($info->getPathname()))?></td>
            </tr>
        <?php elseif(mime_content_type($info->getPathname()) === 'image/x-ms-bmp'): ?>
            <tr>
                <td><img src="icons/image_other.svg" width="20"><?=basename($info->getPathname());?></td>
                <td><?='BMP'?></td>
                <td><?=number_format((float)filesize($info->getPathname())/1024, 2, '.', ''). ' Kb';?></td>
                <td><?=date("F d Y H:i:s.", fileatime($info->getPathname()))?></td>
            </tr>
        <?php elseif(substr(mime_content_type($info->getPathname()),0,5) === 'image'): ?>
            <tr>
                <td><img src="icons/image_other.svg" width="20"><?=basename($info->getPathname());?></td>
                <td><?='Unknown image type'?></td>
                <td><?=number_format((float)filesize($info->getPathname())/1024, 2, '.', ''). ' Kb';?></td>
                <td><?=date("F d Y H:i:s.", fileatime($info->getPathname()))?></td>
            </tr>
        <?php endif;?>
    <?php endforeach;?>
</table>
</body>
</html>