<?php

$directory = new \RecursiveDirectoryIterator(__DIR__,FilesystemIterator::FOLLOW_SYMLINKS);
$filter = new \RecursiveCallbackFilterIterator($directory, function ($current, $key, $iterator) {
    if (
        substr($current->getFilename(),0,2)  === '..' ||
        $current->getFilename() === 'index.php' ||
        $current->getFilename() === 'scanner.php'
    ) {
        return false;
    }
    if (
        $current->getFilename() === '.gitignore' ||
        $current->getFilename() === '.idea' ||
        $current->getFilename() === 'icons'
    ){
        return false;
    }

    return $current->getFilename();
});
$iterator = new \RecursiveIteratorIterator($filter);