<?php
function showTree($folder, $space)
{
    /* list of files in the $folder */
    $files = scandir($folder);
    foreach ($files as $file) {
        /* Directories and files filter */
        if (
            ($file == '.') || ($file == '..') || ($file == '.gitignore') || ($file == '.idea') ||
            ($file == 'icons') || ($file == 'index.php') || ($file == '')
        ) {
            continue;
        }
        $f0 = $folder . '/' . $file; //full path to file
        /* if dir print dir info and call recursive function  */
        if (is_dir($f0)) {

            echo $space . '<img src="icons/directory.svg" width="20">' . $file . "<strong> Directory</strong><br />";

            showTree($f0, $space . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
        } /* if file print info depends on file type*/
        else {
            if (mime_content_type($f0) === 'text/plain') {
                echo $space . '<img src="icons/textfile.svg" width="20">' .
                    $file . " TXT " . number_format((float)filesize($f0) / 1024, 2, '.', '') .
                    " Kb Modified at: " . date("F d Y H:i:s.", fileatime($f0)) . "<br />";
            } else if (mime_content_type($f0) === 'image/png') {
                echo $space . '<img src="icons/image_png.svg" width="20">' .
                    $file . " PNG " . number_format((float)filesize($f0) / 1024, 2, '.', '') .
                    " Kb Modified at: " . date("F d Y H:i:s.", fileatime($f0)) . "<br />";
            } else if (mime_content_type($f0) === 'image/jpeg') {
                echo $space . '<img src="icons/image_jpg.svg" width="20">' .
                    $file . " JPG " . number_format((float)filesize($f0) / 1024, 2, '.', '') .
                    " Kb Modified at: " . date("F d Y H:i:s.", fileatime($f0)) . "<br />";
            } elseif (substr(mime_content_type($f0), 0, 5) === 'image') {
                echo $space . '<img src="icons/image_other.svg" width="20">' .
                    $file . " IMAGE " . number_format((float)filesize($f0) / 1024, 2, '.', '') .
                    " Kb Modified at: " . date("F d Y H:i:s.", fileatime($f0)) . "<br />";
            }

        }
    }
}

showTree("./", "");